import { Component, OnInit } from '@angular/core';
import { PollService } from '../../../core/services/poll.service';

@Component({
	selector: 'app-success',
	templateUrl: './success.component.html',
	styleUrls: ['./success.component.scss'],
})
export class SuccessComponent implements OnInit {
	constructor(public pollService: PollService) {}

	ngOnInit(): void {}
}
