import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PollResultsCompactComponent } from './poll-results-compact.component';

describe('PollResultsCompactComponent', () => {
	let component: PollResultsCompactComponent;
	let fixture: ComponentFixture<PollResultsCompactComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [PollResultsCompactComponent],
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(PollResultsCompactComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
