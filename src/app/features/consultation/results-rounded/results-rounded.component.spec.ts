import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultsRoundedComponent } from './results-rounded.component';

describe('ResultsRoundedComponent', () => {
	let component: ResultsRoundedComponent;
	let fixture: ComponentFixture<ResultsRoundedComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ResultsRoundedComponent],
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ResultsRoundedComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
