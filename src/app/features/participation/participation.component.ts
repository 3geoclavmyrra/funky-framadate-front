import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';

import { Poll } from '../../core/models/poll.model';
import { Owner } from '../../core/models/owner.model';
import { ModalService } from '../../core/services/modal.service';
import { UserService } from '../../core/services/user.service';
import { SettingsComponent } from '../../shared/components/settings/settings.component';

@Component({
	selector: 'app-participation',
	templateUrl: './participation.component.html',
	styleUrls: ['./participation.component.scss'],
})
export class ParticipationComponent implements OnInit, OnDestroy {
	public poll: Poll;
	public _user: Observable<Owner> = this.userService.user;
	private routeSubscription: Subscription;

	constructor(
		private router: Router,
		private activatedRoute: ActivatedRoute,
		private userService: UserService,
		private modalService: ModalService
	) {}

	ngOnInit(): void {
		if (!this.userService.isCurrentUserIdentifiable()) {
			this.modalService.openModal(SettingsComponent);
		}
		this.routeSubscription = this.activatedRoute.data.subscribe((data: { poll: Poll }) => {
			if (data.poll) {
				this.poll = data.poll;
			} else {
				this.router.navigate(['/page-not-found']);
			}
		});
	}

	ngOnDestroy(): void {
		if (this.routeSubscription) {
			this.routeSubscription.unsubscribe();
		}
	}
}
