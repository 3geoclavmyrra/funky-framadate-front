import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

import { SharedModule } from '../../shared/shared.module';
import { AdministrationRoutingModule } from './administration-routing.module';
import { AdministrationComponent } from './administration.component';
import { StepperComponent } from './stepper/stepper.component';
import { NamingComponent } from './naming/naming.component';
import { FormComponent } from './form/form.component';
import { StepOneComponent } from './form/steps/step-one/step-one.component';
import { StepTwoComponent } from './form/steps/step-two/step-two.component';
import { StepThreeComponent } from './form/steps/step-three/step-three.component';
import { StepFourComponent } from './form/steps/step-four/step-four.component';
import { StepFiveComponent } from './form/steps/step-five/step-five.component';
import { CalendarModule } from 'primeng/calendar';
import { SuccessComponent } from './success/success.component';
import { DateSelectComponent } from './form/date-select/date-select.component';
import { TextSelectComponent } from './form/text-select/text-select.component';
import { KindSelectComponent } from './form/kind-select/kind-select.component';
import { BaseConfigComponent } from './form/base-config/base-config.component';
import { AdvancedConfigComponent } from './form/advanced-config/advanced-config.component';

import { DragDropModule } from '@angular/cdk/drag-drop';
import { IntervalComponent } from './form/date/interval/interval.component';
import { DayListComponent } from './form/date/list/day/day-list.component';
import { PickerComponent } from './form/date/picker/picker.component';
import { TimeListComponent } from './form/date/list/time/time-list.component';
import { AdminConsultationComponent } from './consultation/consultation.component';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { StepSixComponent } from './form/steps/step-six/step-six.component';
import { StepSevenComponent } from './form/steps/step-seven/step-seven.component';
import { OptionLinkComponent } from './form/option-link/option-link.component';
import { TextListComponent } from './form/text-list/text-list.component';
import { HoursComponent } from './form/hours/hours.component';
import { ActionsMenuComponent } from './actions-menu/actions-menu.component';
import { NavStepsComponent } from './nav-steps/nav-steps.component';
import { DialogModule } from 'primeng/dialog';
import { CoreModule } from '../../core/core.module';

@NgModule({
	declarations: [
		AdministrationComponent,
		StepperComponent,
		NamingComponent,
		FormComponent,
		SuccessComponent,
		DateSelectComponent,
		TextSelectComponent,
		KindSelectComponent,
		BaseConfigComponent,
		AdvancedConfigComponent,
		StepOneComponent,
		StepTwoComponent,
		StepThreeComponent,
		StepFourComponent,
		StepFiveComponent,
		SuccessComponent,
		IntervalComponent,
		DayListComponent,
		PickerComponent,
		TimeListComponent,
		AdminConsultationComponent,
		StepSixComponent,
		StepSevenComponent,
		OptionLinkComponent,
		TextListComponent,
		HoursComponent,
		ActionsMenuComponent,
		NavStepsComponent,
	],
	imports: [
		AdministrationRoutingModule,
		CommonModule,
		CalendarModule,
		ReactiveFormsModule,
		SharedModule,
		FormsModule,
		TranslateModule.forChild({ extend: true }),
		DragDropModule,
		ConfirmDialogModule,
		DialogModule,
	],
	exports: [ActionsMenuComponent, StepperComponent, NavStepsComponent],
})
export class AdministrationModule {}
