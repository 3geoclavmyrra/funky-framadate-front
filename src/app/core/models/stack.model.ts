import { Vote } from './vote.model';
import { Owner } from './owner.model';

export class Stack {
	public id: number;
	public custom_url: string;
	public pass_hash: string;
	public pseudo = 'Choque Nourrice';
	public comment = 'Le beau commentaire tout neuf';
	public owner: Owner = new Owner();
	public votes: Vote[] = [];
}
