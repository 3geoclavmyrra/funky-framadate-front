import { Injectable } from '@angular/core';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';

import { LanguageEnum } from '../enums/language.enum';
import { StorageService } from './storage.service';
import { environment } from '../../../environments/environment';

@Injectable({
	providedIn: 'root',
})
export class LanguageService {
	constructor(private translate: TranslateService, private storageService: StorageService) {}

	public getLangage(): LanguageEnum {
		return this.translate.currentLang as LanguageEnum;
	}

	public setLanguage(language: string): void {
		this.translate.use(language);
	}

	public getAvailableLanguages(): string[] {
		return this.translate.getLangs();
	}

	public configureAndInitTranslations(): void {
		// always save in storage the currentLang used
		this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
			this.storageService.language = event.lang as LanguageEnum;
		});

		// set all languages available
		this.translate.addLangs(Object.keys(LanguageEnum));

		// set language
		this.setLanguageOnInit();
	}

	public getPrimeNgStrings() {
		return this.translate.get('calendar_widget');
	}

	private setLanguageOnInit(): void {
		// set language from storage
		if (!this.translate.currentLang) {
			this.setLanguageFromStorage();
		}

		// or set language from browser
		if (!this.translate.currentLang) {
			this.setLanguageFromBrowser();
		}

		// set default language
		if (!this.translate.currentLang) {
			this.setLanguage(LanguageEnum[environment.defaultLanguage]);
		}
	}

	private setLanguageFromStorage(): void {
		if (this.storageService.language && this.translate.getLangs().includes(this.storageService.language)) {
			this.setLanguage(this.storageService.language);
		}
	}

	private setLanguageFromBrowser(): void {
		const currentBrowserLanguage: LanguageEnum = this.translate.getBrowserLang() as LanguageEnum;
		console.log('currentBrowserLanguage', currentBrowserLanguage);
		if (this.translate.getLangs().includes(currentBrowserLanguage)) {
			this.setLanguage(currentBrowserLanguage);
		}
	}
}
