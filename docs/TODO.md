# Todo - liste des choses à faire
## Idées pour de futures améliorations (pertinence à vérifier)

* Gagner en vie privée en chiffrant certaines informations ? Stockage zéro knowledge avec un chiffrement robuste à courbe elliptique.
* À réfléchir : permettre à Framadate de faire entrer à des gens plusieurs plages de temps de disponibilité et le service déduit quelles sont les plages de temps favorables (calcul d'intersection sur des lignes discontinues). Cela pourrait être avec divers niveaux de détail. Comme https://omnipointment.com/ (qui est un logiciel privateur)
* SSO du fédiverse ?

# Tableau des avancements
https://framagit.org/framasoft/framadate/funky-framadate-front/-/boards


# Choses faites
voir le [changelog.md]
Les idées et rapports de bugs sont disponibles dans [le tableau des Issues](https://framagit.org/framasoft/framadate/funky-framadate-front/-/boards) sur la forge logicielle.

## principales

* Accessibilité renforcée.
* Adapté aussi bien sur mobile que grands écrans à haute ou faible densité de pixels.
* Tests unitaires et end-to-end.
* Couverture de test.
* Choix de réponses possibles. Proposer de ne répondre que « oui » ou rien, ou aller dans la nuance en proposant « oui », « peut-être », « non », « ? ». *# Redondance ou le choix de réponses possibles de la première phrase concerne un autre choix ?*
* Insertion d'images dans le sondage de type texte, avec des URL uniquement. Une seule image par title possible ou rien.
* Thème sombre.
* Duplication de sondage à partir d'un autre.
* Limiter le nombre de participants maximum


